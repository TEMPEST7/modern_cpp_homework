#! /usr/bin/bash

echo Ans 1: $(cat data.dat | wc -l)
echo Ans 2: $(cat data.dat | grep d[ao]lor | wc -l)
echo Ans 3: $(cat data.dat | wc -w)
echo Ans 4: $(cat data.dat | grep " mol" | wc -l)
