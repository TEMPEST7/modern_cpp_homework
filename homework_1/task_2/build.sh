#! /usr/bin/bash

rm -rf ./build
mkdir build/
cd build/

output_file=main
echo compiliing objects
c++ -I ./include/ -c src/subtract.cpp -o build/subtract.o
c++ -I ./include/ -c src/sum.cpp -o build/sum.o
echo making modules from objects
ar rcs libarithmetic.a /home/tempest/modern_cpp_homework/homework_1/task_2/results/lib/subtract.o /home/tempest/modern_cpp_homework/homework_1/task_2/results/lib/sum.o 
echo building
c++ -std=c++17 /home/tempest/modern_cpp_homework/homework_1/task_2/src/main.cpp -L . -larithmetic -o /home/tempest/modern_cpp_homework/homework_1/task_2/results/bin/$output_file -I /home/tempest/modern_cpp_homework/homework_1/task_2/include
echo Done. Press enter to run program or any other key to exit.
read input
if [ "$input" = "" ]; then
    /home/tempest/modern_cpp_homework/homework_1/task_2/results/bin/$output_file
else
    echo Exiting...
fi
