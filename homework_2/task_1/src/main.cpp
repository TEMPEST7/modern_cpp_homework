#include <iostream>
#include <random>


int main(){
    std::random_device rd;
    int secret_num = rd() % 100;
    int guess;

    while (guess!=secret_num) {
        std::cout << "Guess the number: ";
        std::cin >> guess;
        if(std::cin.fail()) {
            std::cerr << "Error encountered, exiting..." << std::endl;
            std::cout << secret_num << std::endl;
            return EXIT_FAILURE;
        } else if ((guess<99 && guess>0)){
            if (guess>secret_num) {
                std::cout << "The secret number is lower!" << std::endl;
            } else {
                std::cout << "The secret number is higher!" << std::endl;
            }
        } else {
            std::cerr << "[WARNING] : Number must be between 0 and 99" << std::endl;
        }

    }

    std::cout << "Yes! The secrete number is "<<secret_num << std::endl;

    return EXIT_SUCCESS;
}