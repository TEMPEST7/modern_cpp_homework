#include "tools.hpp"

bool compatibility_check(int file_count, const char *filenames[]) {
  for (int i = 1; i < file_count; i++) {
    std::string arg = filenames[i];
    std::string ext = arg.substr(arg.find("."), arg.size());
    std::string name = arg.substr(0, arg.find("."));
    if (ext != ".txt" && ext != ".png") {
      return false;
    }
    for (int j = 0; j < name.length(); j++)
      if (isdigit(name[j]) == false)
        return false;
  }
  return true;
}
