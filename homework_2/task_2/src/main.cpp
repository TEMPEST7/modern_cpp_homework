#include <tools.hpp>

int main(int argc, char const *argv[]) {
  if (argc == 3) {
    if (!compatibility_check(argc, argv)) {
      std::cerr << "Unsupported file format." << std::endl;
      exit(EXIT_FAILURE);
    }

    std::string f1_name = argv[1];
    std::string f2_name = argv[2];

    file f1 = {f1_name.substr(0, f1_name.find(".")),
               f1_name.substr(f1_name.find("."), f1_name.size())};
    file f2 = {f2_name.substr(0, f2_name.find(".")).c_str(),
               f2_name.substr(f2_name.find("."), f2_name.size())};

    if (f1.ext == ".txt" && f2.ext == ".txt") {
      std::cout << double(atoi(f1.name.c_str()) + atoi(f2.name.c_str())) / 2
                << std::endl;
    } else if (f1.ext == ".png" && f2.ext == ".png") {
      std::cout << (atoi(f1.name.c_str()) + atoi(f2.name.c_str())) << std::endl;
    } else if (f1.ext == ".txt" && f2.ext == ".png") {
      std::cout << (atoi(f1.name.c_str()) % atoi(f2.name.c_str())) << std::endl;
    }
  } else {
    std::cerr << "Exactly 2 arguments allowed." << std::endl;
    exit(EXIT_FAILURE);
  }

  return 0;
}
