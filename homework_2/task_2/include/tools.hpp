#include <iostream>
#include <string>

struct file {
  std::string name, ext;
};

/// @brief Checks for compatible filename extentions
/// @param file_count
/// @param filenames
/// @return bool
bool compatibility_check(int file_count, const char *filenames[]);